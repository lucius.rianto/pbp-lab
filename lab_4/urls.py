from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='Note'),
    path('add-note/', add_note, name='Note Form'),
    path('note-list/', note_list, name='Note Card List'),
]
