from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta: 
        model = Note
        fields = "__all__"
        widgets={
            'to' : forms.TextInput(attrs={'class' :'form-control', 'placeholder':'Recipient'}),
            'From' : forms.TextInput(attrs={'class' :'form-control', 'placeholder':'Sender'}),
            'title' : forms.TextInput(attrs={'class' :'form-control', 'placeholder':'Message Title'}),
            'message' : forms.Textarea(attrs={'class' :'form-control', 'placeholder':'Your Message...'}),
        }