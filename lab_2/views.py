from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note

# HTML view
def index(request):
    note = Note.objects.all()
    response = {'notes': note}
    return render(request, 'lab2.html', response)

# XML view
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# JSON view
def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")