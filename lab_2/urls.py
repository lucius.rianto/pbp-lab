from django.urls import path
from .views import index
from .views import xml
from .views import json

urlpatterns = [
    path('', index, name='Note'),
    path('xml/', xml, name='Note XML'),
    path('json/', json, name='Note JSON'),
]
