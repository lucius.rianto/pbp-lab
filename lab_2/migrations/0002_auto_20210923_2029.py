# Generated by Django 3.2.7 on 2021-09-23 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='frm',
            new_name='From',
        ),
        migrations.AlterField(
            model_name='note',
            name='title',
            field=models.CharField(default='', max_length=30),
        ),
    ]
