// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0,
          title: const Text("CovidCares Login"),
          backgroundColor: Colors.transparent,
        ),
        body: Container(
            height: 1000,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Colors.red,
                Colors.orange,
                Colors.yellow,
                Colors.green,
                Colors.blue,
                Colors.indigo,
                Colors.purple,
              ],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
            )),
            child: Center(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: "example : luciusrianto13",
                              labelText: "Username",
                              icon: const Icon(Icons.people),
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Username wajib diisi';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            obscureText: true,
                            decoration: new InputDecoration(
                              labelText: "Password",
                              icon: const Icon(Icons.vpn_key),
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Password wajib diisi';
                              }
                              return null;
                            },
                          ),
                        ),
                        CheckboxListTile(
                          title: const Text('Remember me'),
                          subtitle: const Text('Saves login credential'),
                          value: nilaiCheckBox,
                          activeColor: Colors.amberAccent,
                          onChanged: (value) {
                            setState(() {
                              nilaiCheckBox = value!;
                            });
                          },
                        ),
                        RaisedButton(
                          child: const Text(
                            "Login",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.amberAccent,
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {}
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }
}
