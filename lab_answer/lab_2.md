1. Apakah perbedaan antara JSON dan XML?

    1. XML adalah markup language sedangkan JSON adalah sebuah format penulisan dari JavaScript.
    2. XML menyimpan datanya didalam sebuah tree structure sedangkan data pada JSON disimpan seperti map pada Java.
    3. XML relatif besar dan lambat sedangkan JSON jauh lebih cepat karena ukurannya yang lebih kecil.
    4. XML mendukung penggunaan dari namespace dan comment sedangkan JSON tidak mendukung keduanya
    5. XML mendukung lebih banyak tipe data sedangkan JSON hanya mendukung data primitif dan sedikit data kompleks.
    6. XML lebih rentan secara keamanannya daripada JSON.

2. Apakah perbedaan antara HTML dan XML?

    1. Fokus dari HTML adalah penyajain data sedangkan XML lebih berfokus pada transfer data
    2. XML menyediakan dukungan namespace dan comment sedangkan HTML tidak
    3. XML mewajibkan penggunaan dari tag penutup untuk semua tag yang dibuka sedangkan HTML tidak semua harus ditutup dengan tag penutup
    4. XML memiliki tag yang tak terbatas karena setiap tag diciptakan oleh penggunanya masing-masing sedangkan tag pada HTML terbatas karena sudah tersedia dari awal
    5. XML bersifat case-sensitive sedangkan HTML tidak, ini artinya pada XML 'a' dan 'A' berbeda sedangkan bagi HTML sama
