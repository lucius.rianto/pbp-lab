from django.urls import path
from .views import *

urlpatterns = [
    path('', login_view, name='login_view'),
    path('add/', add_friend, name='add_friend'),
    path('friends/', index, name='index'),
]