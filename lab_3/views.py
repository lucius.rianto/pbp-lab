from django.shortcuts import redirect, render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.
def login_view(request):
    login_form = AuthenticationForm(data=request.POST)
    if login_form.is_valid():
        return redirect(add_friend)
    return render(request,'lab3_login.html', {'login_form':login_form})

def add_friend(request):
    context = {}

    friendform = FriendForm(request.POST or None, request.FILES or None)

    if friendform.is_valid():
        friendform.save()
        return redirect(index)

    context['form']= friendform
    return render(request, 'lab3_form.html', context)

def index(request):
    friend = Friend.objects.all()
    response = {'friends': friend}
    return render(request, 'friend_list_lab1.html', response)
