import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: 'Covid Cares Login',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    ));

class LoginScreen extends StatelessWidget (
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff2ACAEA),
      body: Container(
        child:  Column(
          children: <Widget>[
            Container(
              height: 350,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/background.png'),
                  fit: BoxFit.fill
                )
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    width: 20,
                    height: 200,
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/light-1.png'),
                        )
                      ),
                    )
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }
)
