from datetime import timezone
from django.db import models
from django.utils import timezone

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    name = models.CharField(max_length=30,default="")
    npm = models.CharField(max_length=10,default="")
    dob = models.DateField(default=timezone.now)

    def __str__(self):
        return self.name